import * as React from 'react';
import './App.css';
import Banner from './components/Banner';
import Bar from './components/Bar';
import Box from './components/Box';
// import Button from './components/Button';
import Button2 from './components/Button2';
import Card from './components/Card';
import HelloWorld from './components/HelloWorld';


class App extends React.Component {
  public render() {
    return (
      <React.Fragment>
        <HelloWorld defaultName="Marcelino"/>
        {/* <Button></Button> */}
        <Button2 onClick={ this.handleButtonClick }>
          <div>Pincha aquí</div>
        </Button2>
        <Box defaultTitleWrapper="Holititis" type="blue"/>
        <Banner text="patata frita"/>
        <Bar label="hola"/>
        <Card />
      </React.Fragment>
    );
  }
  private handleButtonClick() {
    alert('jojojo');
  }
}

export default App;
