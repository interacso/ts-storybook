import React, { PureComponent } from 'react';
import { defaultThumbnail } from '../../assets';
import { getThumbnailUrl } from '../../utils';

import {
  Container,
  Detail,
  ImageContainer,
  Img,
  Response,
  TemplateAutor,
  Title,
} from './styles';

interface IProps {
    _id?: string,
    campaign?: any,
    name?: string,  
}
export default class Card extends PureComponent<IProps> {
  public render() {
    const { _id, name, campaign } = this.props;
    const thumb = getThumbnailUrl(_id);
    return (
      <Container key={`cards_fake_${_id}`}>
        <ImageContainer>
          <Img src={thumb || defaultThumbnail} />
        </ImageContainer>
        <Detail>
          <Title>{name}</Title>
          <TemplateAutor>
            Template:
            <Response> {campaign && campaign.name} </Response>{' '}
          </TemplateAutor>
          <TemplateAutor>
            Author: <Response>{campaign && campaign.advertiser.name}</Response>
          </TemplateAutor>
        </Detail>
      </Container>
    );
  }
}
