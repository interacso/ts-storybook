import React, { MouseEvent, ReactNode } from 'react';

interface IProps {
  children?: ReactNode;
  onClick(e: MouseEvent<HTMLElement>): void
}

const Button = ({ onClick: handleClick, children }: IProps) => (
  <button onClick={handleClick}>{children}</button>
)

export default Button;