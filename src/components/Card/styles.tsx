import styled from 'styled-components';
import { colors, sizes, fontWeights } from '../../theme';

export const Container = styled.div`
  background-color: ${colors.white};
  border-radius: 6px;
  box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.2);
  width: 100%;
  object-fit: cover;
  min-width: 194px;
  max-width: 194px;
  height: 430px;
`;

export const ImageContainer = styled.div`
  background-color: ${colors.megaLightGrey};
  padding: 2px;
`;

export const Detail = styled.div`
  border-top: 1px solid ${colors.superlightGrey};
  padding: 10px;
`;

export const Title = styled.h2`
  color: ${colors.superDarkGrey};
  font-size: ${sizes.S};
  font-weight: ${fontWeights.semiBold};
  height: 28px;
  line-height: 28px;
  margin-bottom: 0px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export const TemplateAutor = styled.p`
  height: 18px;
  color: ${colors.lightGrey};
  font-size: ${sizes.XS};
  line-height: 18px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  margin: 0px;
`;

export const Response = styled.span`
  height: 18px;
  width: 123px;
  color: ${colors.superDarkGrey};
  font-size: ${sizes.XS};
  line-height: 18px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export const Img = styled.img`
  height: 346px;
  width: 100%;
  background-repeat: no-repeat;
`;
