import { NavLink } from 'react-router-dom';
import styled from 'styled-components';

const activeClassName = 'active';

interface IProps {
  disabled: boolean;
}

const DefaultLink = styled(NavLink)`
  text-decoration: none;
  font-size: 14px;
  font-weight: 400;
  pointer-events: ${(props: IProps) => (props.disabled ? 'none' : 'auto')};
  &:hover {
    text-decoration: none;
  }
  &.${activeClassName} {
    text-decoration: none;
  }
`;

export const BasicLink = styled(DefaultLink)`
  color: blue;
  &:hover {
    color: green;
  }
  &.${activeClassName} {
    color: blue;
  }
`;

export const BlackLink = styled(DefaultLink)`
  color: grey;
  &:hover {
    color: orange;
  }
  &.${activeClassName} {
    color: black;
  }
`;