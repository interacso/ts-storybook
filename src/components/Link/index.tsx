import React, { Component, Fragment } from 'react';
import { BasicLink, BlackLink } from './styles';

interface IProps {
  type?: string,
  to?: string,
  text?: string,
  disabled: boolean,
};

class LinkStyle extends Component<IProps> {
  public render() {
    const { type, text, disabled, to } = this.props;
    if (type === 'header') {
      return (
        <Fragment>
          <BlackLink disabled={disabled} to={to || ''}>{text}</BlackLink>
        </Fragment>
      );
    }
    return (
      <Fragment>
        <BasicLink disabled={disabled} to={to || ''}>{text}</BasicLink>
      </Fragment>
    );
  }
}

export default LinkStyle;