// import styled from 'styled-components';
// import { Button } from 'antd';
// import { colors, sizes, fontWeights } from '../../theme';

// const Basicbutton = styled(Button)`
//   min-height: 48px;
//   padding: 0 30px;
//   border-radius: 4px;
//   font-size: ${sizes.M};
//   font-weight: ${fontWeights.semiBold};
// `;
// export const BlueButton = styled(Basicbutton)`
//   background-color: ${colors.blue};
//   color: ${colors.white};
//   &:hover {
//     background-color: ${colors.darkBlue};
//     color: ${colors.white};
//   }
//   &:disabled {
//     background-color: ${colors.superLightBlue};
//     color: ${colors.white};
//   }
//   &:active {
//     background-color: ${colors.superDarkBlue};
//     color: ${colors.white};
//   }
//   &:focus {
//     background-color: ${colors.superDarkBlue};
//     color: ${colors.white};
//   }
// `;
// export const GreyButton = styled(Basicbutton)`
//   background-color: ${colors.darkGrey};
//   color: ${colors.white};
//   &:hover {
//     background-color: ${colors.grey};
//     color: ${colors.white};
//   }
//   &:disabled {
//     background-color: ${colors.superlightGrey};
//     color: ${colors.white};
//   }
//   &:active {
//     background-color: ${colors.superDarkGrey};
//     color: ${colors.white};
//   }
//   &:focus {
//     background-color: ${colors.superDarkGrey};
//     color: ${colors.white};
//   }
// `;
// export const RedButton = styled(Basicbutton)`
//   background-color: ${colors.red};
//   color: ${colors.white};
//   &:hover {
//     background-color: ${colors.darkRed};
//     color: ${colors.white};
//   }
//   &:disabled {
//     background-color: ${colors.lightRed};
//     color: ${colors.white};
//   }
//   &:active {
//     background-color: ${colors.superDarkRed};
//     color: ${colors.white};
//   }
//   &:focus {
//     background-color: ${colors.superDarkRed};
//     color: ${colors.white};
//   }
// `;
// export const WhiteBlueButton = styled(Basicbutton)`
//   background-color: ${colors.white};
//   color: ${colors.blue};
//   border: 1px solid ${colors.blue};
//   &:hover {
//     background-color: ${colors.lightGrey};
//     color: ${colors.blue};
//     border: 1px solid ${colors.blue};
//   }
//   &:disabled {
//     background-color: ${colors.white};
//     color: ${colors.superLightBlue};
//     border: 1px solid ${colors.superLightBlue};
//   }
//   &:active {
//     background-color: ${colors.white};
//     color: ${colors.superDarkBlue};
//     border: 1px solid ${colors.superDarkBlue};
//   }
//   &:focus {
//     background-color: ${colors.white};
//     color: ${colors.superDarkBlue};
//     border: 1px solid ${colors.superDarkBlue};
//   }
// `;
// export const WhiteRedButton = styled(Basicbutton)`
//   background-color: ${colors.white};
//   color: ${colors.red};
//   border: 1px solid ${colors.white};
//   &:hover {
//     background-color: ${colors.lightGrey};
//     color: ${colors.red};
//     border: 1px solid ${colors.white};
//   }
//   &:disabled {
//     background-color: ${colors.white};
//     color: ${colors.red};
//     border: 1px solid ${colors.white};
//   }
//   &:active {
//     background-color: ${colors.white};
//     color: ${colors.superDarkRed};
//     border: 1px solid ${colors.white};
//   }
//   &:focus {
//     background-color: ${colors.white};
//     color: ${colors.superDarkRed};
//     border: 1px solid ${colors.white};
//   }
// `;
// export const EmergencySelect = styled(Basicbutton)`
//   min-height: 68px;
//   padding: 0 35px;
//   border: 1px solid ${colors.superDarkBlue};
//   border-radius: 3px;
//   background-color: ${colors.white};
//   box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.33);
//   color: ${colors.superDarkBlue};
//   font-size: ${sizes.L};
//   &:hover {
//     background-color: ${colors.lightGrey};
//   }
//   &:disabled {
//     background-color: ${colors.superlightGrey};
//   }
//   &:active {
//     background-color: ${colors.blue};
//     color: ${colors.superDarkBlue};
//   }
//   &:focus {
//     background-color: ${colors.blue};
//     color: ${colors.superDarkBlue};
//   }
// `;
// export const ResetButton = styled(Basicbutton)`
//   background-color: ${colors.superLightBlue};
//   color: ${colors.blue};
//   border: 1px solid ${colors.superLightBlue};
//   &:hover {
//     background-color: ${colors.superLightBlue};
//     color: ${colors.blue};
//     border: 1px solid ${colors.superLightBlue};
//   }
//   &:disabled {
//     background-color: ${colors.superLightBlue};
//     color: ${colors.lightBlue};
//     border: 1px solid ${colors.superLightBlue};
//   }
//   &:active {
//     background-color: ${colors.superLightBlue};
//     color: ${colors.blue};
//     border: 1px solid ${colors.superLightBlue};
//   }
//   &:focus {
//     background-color: ${colors.superLightBlue};
//     color: ${colors.blue};
//     border: 1px solid ${colors.superLightBlue};
//   }
// `;
// export const ResetBlueWhiteButton = styled(WhiteBlueButton)`
//   border: 1px solid ${colors.white};
//   &:hover {
//     background-color: ${colors.white};
//     border: 1px solid ${colors.white};
//   }
//   &:disabled {
//     background-color: ${colors.white};
//     border: 1px solid ${colors.white};
//   }
//   &:active {
//     background-color: ${colors.white};
//     border: 1px solid ${colors.white};
//   }
//   &:focus {
//     background-color: ${colors.white};
//     border: 1px solid ${colors.white};
//   }
// `;
