import * as React from 'react';

interface IMyComponentProps {
  defaultName?: string
}

interface IMyComponentState {
  name?: string
}

export default class HelloWorld extends React.Component<IMyComponentProps, IMyComponentState> {
  constructor(props: IMyComponentProps){
    super(props);
    this.state = { name: this.props.defaultName};
  }
  
  public render() {
    return (
      <div>
        Hello { this.state.name }!
      </div>
    );
  }
}