import React, { PureComponent } from 'react';
import {
  BodyWrapper,
  BodyWrapperBlue,
  BodyWrapperBlueAlert,
  HeaderWrapper,
  HeaderWrapperBlue,
  SelectWrapper,
  SelectWrapperBlue,
  SelectWrapperBlueAlert,
  SelectWrapperGreen,
  SelectWrapperOrange,
  TitleWrapper,
  TitleWrapperBlack,
} from './styles';

interface IProps {
  defaultTitleWrapper?: string,
  type?: string,
}

interface IState {
  titleWrapper?: string
}

export default class Box extends PureComponent<IProps, IState> {
  constructor(props: IProps){
    super(props);
    this.state = { 
      titleWrapper: this.props.defaultTitleWrapper
    };
  }
  public renderBox(type: string, children: any) {
    switch (type) {
      case 'blue': {
        return (
          <SelectWrapperBlue>
            <HeaderWrapperBlue>
              <TitleWrapperBlack> {this.state.titleWrapper} </TitleWrapperBlack>
            </HeaderWrapperBlue>
            <BodyWrapperBlue>{children}</BodyWrapperBlue>
          </SelectWrapperBlue>
        );
      }
      case 'green': {
        return (
          <SelectWrapperGreen>
            <HeaderWrapperBlue>
              <TitleWrapper> {this.state.titleWrapper} </TitleWrapper>
            </HeaderWrapperBlue>
          </SelectWrapperGreen>
        );
      }
      case 'orange': {
        return (
          <SelectWrapperOrange>
            <HeaderWrapperBlue>
              <TitleWrapper> {this.state.titleWrapper} </TitleWrapper>
            </HeaderWrapperBlue>
          </SelectWrapperOrange>
        );
      }
      case 'blue-alert': {
        return (
          <SelectWrapperBlueAlert>
            <HeaderWrapperBlue>
              <TitleWrapper> {this.state.titleWrapper} </TitleWrapper>
            </HeaderWrapperBlue>
            <BodyWrapperBlueAlert>{children}</BodyWrapperBlueAlert>
          </SelectWrapperBlueAlert>
        );
      }
      default:
        return (
          <SelectWrapper>
            <HeaderWrapper>
              <TitleWrapperBlack> {this.state.titleWrapper} </TitleWrapperBlack>
            </HeaderWrapper>
            <BodyWrapper>{children}</BodyWrapper>
          </SelectWrapper>
        );
    }
  }
  public render() {
    const { type="", children } = this.props;
    return <div>{this.renderBox(type, children)}</div>;
  }
}
