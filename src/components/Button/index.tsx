// import React, { PureComponent } from 'react';
// import {
//   BlueButton,
//   GreyButton,
//   RedButton,
//   WhiteBlueButton,
//   WhiteRedButton,
//   EmergencySelect,
//   ResetButton,
//   ResetBlueWhiteButton,
// } from './styles.js';

// type IProps = {
//   type?: string,
//   button?: string,
// };

// class Button extends PureComponent<IProps> {
//   public render() {
//     const { type } = this.props;
//     if (type === 'warning') {
//       return <WhiteRedButton >{this.props.button}</WhiteRedButton>;
//     }
//     if (type === 'grey') {
//       return <GreyButton >{this.props.button}</GreyButton>;
//     }
//     if (type === 'red') {
//       return <RedButton >{this.props.button}</RedButton>;
//     }
//     if (type === 'whiteBlue') {
//       return <WhiteBlueButton >{this.props.button}</WhiteBlueButton>;
//     }
//     if (type === 'whiteRed') {
//       return <WhiteRedButton >{this.props.button}</WhiteRedButton>;
//     }
//     if (type === 'emergencySelect') {
//       return <EmergencySelect >{this.props.button}</EmergencySelect>;
//     }
//     if (type === 'reset') {
//       return <ResetButton >{this.props.button}</ResetButton>;
//     }
//     if (type === 'resetBlueWhite') {
//       return (
//         <ResetBlueWhiteButton >
//           {this.props.button}
//         </ResetBlueWhiteButton>
//       );
//     }

//     return <BlueButton >{this.props.button}</BlueButton>;
//   }
// }
// export default Button;