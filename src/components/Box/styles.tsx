import styled from 'styled-components';

export const SelectWrapper = styled.div`
  height: auto;
  width: 100%;
  border-radius: 4px;
  background-color: white;
  box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.2);
`;
export const SelectWrapperBlue = styled(SelectWrapper)`
  background-color: blue;
`;
export const SelectWrapperOrange = styled(SelectWrapper)`
  background-color: red;
`;
export const SelectWrapperYellow = styled(SelectWrapper)`
  background-color: yellow;
`;
export const SelectWrapperGreen = styled(SelectWrapper)`
  background-color: green;
`;
export const SelectWrapperBlueAlert = styled(SelectWrapper)`
  background-color: blue;
`;
export const HeaderWrapper = styled.div`
  height: 48px;
  background-color: grey;
  border-radius: 4px 4px 0px 0px;
`;
export const HeaderWrapperBlue = styled(HeaderWrapper)`
  height: 48px;
  background-color: blue;
`;
export const BodyWrapper = styled.div`
  background-color: white;
  padding: 20px;
  border-radius: 0px 0px 4px 4px;
`;
export const BodyWrapperBlue = styled(BodyWrapper)`
  background-color: grey;
  padding: 20px;
`;
export const BodyWrapperBlueAlert = styled(BodyWrapper)`
  background-color: blue;
  padding: 20px;
`;
export const TitleWrapper = styled.p`
  color: red;
  font-size: 16px;
  font-weight: 600;
  line-height: 4px;
  padding: 25px;
`;
export const TitleWrapperBlack = styled(TitleWrapper)`
  color: white;
`;
export const StatusText = styled.p`
  color: grey;
  font-size: 20px;
  font-weight: 600;
  line-height: 19px;
  padding: 25px;
`;
export const StatusTextBlue = styled(StatusText)`
  background-color: blue;
`;