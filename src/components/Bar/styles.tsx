import styled from 'styled-components';
export const BarCustom = styled.div`
  margin: 20px 0px 0px;
  height: 1px;
  background: grey;
  border-bottom: 50px solid grey;
`;
export const BarCustomText = styled.p`
  color: blue;
  font-size: 40px;
  font-weight: 600;
  line-height: 4px;
  padding: 25px;
`;
