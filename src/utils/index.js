const {
  REACT_APP_PLAYER,
  REACT_APP_THUMB_SERVER,
  REACT_APP_THUMB_DOMAIN,
  REACT_APP_THUMB_COLLECTION,
} = process.env;
export const api = process.env.REACT_APP_API;
export const localStorageApp = 'cityportal-app';
export const localStorageLang = 'cityportal-app-lang';
export const getToken = () => localStorage.getItem(localStorageApp);
export const setToken = token => localStorage.setItem(localStorageApp, token);
export const setLang = lang => localStorage.setItem(localStorageLang, lang);
export const getLang = () => {
  const defaultUserLang = 'en';
  const languages = ['es', 'en', 'de', 'fr'];
  if (languages.includes(defaultUserLang)) {
    setLang(defaultUserLang);
    return defaultUserLang;
  } else {
    return localStorage.getItem(localStorageLang);
  }
};

const headers = (contentType = 'application/json') => {
  const token = getToken();
  const h = {};
  if (contentType) {
    h['Content-Type'] = contentType;
  }
  h.Accept = 'application/json';
  h.Authorization = `Bearer ${token}`;
  return h;
};

export const request = (
  path: string,
  options = {},
  requestType = 'application/json',
  responseType = 'json'
) => {
  const url = `${api}/${path}`;
  const settings = {
    headers: headers(requestType),
    responseType,
    ...options,
  };
  return fetch(url, settings).then(response => response.json());
};

export const ELEMENTS = {
  TEXT: 'text',
  IMAGE: 'image',
  VIDEO: 'video',
  WEATHER: 'weather',
};

export const getPlayerUrl = creativityId =>
  `${REACT_APP_PLAYER}/${creativityId}?autoplay`;

export const getThumbnailUrl = creativityId =>
  `${REACT_APP_THUMB_SERVER}/?creative=${creativityId}&domain=${REACT_APP_THUMB_DOMAIN}&collection=${REACT_APP_THUMB_COLLECTION}`;
