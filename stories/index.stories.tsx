import * as React from "react";

import { action } from "@storybook/addon-actions";
// import { linkTo } from "@storybook/addon-links";
import { storiesOf } from "@storybook/react";
import { MemoryRouter } from 'react-router'

// const { Welcome } = require("@storybook/react/demo");
import Bar from "../src/components/Bar";
import Box from "../src/components/Box";
import Button from "../src/components/Button2";
import HelloWorld from "../src/components/HelloWorld";
import LinkStyle from "../src/components/Link";

// storiesOf("Welcome", module).add("to Storybook", () => (
//   <Welcome showApp={linkTo("Button")} />
// ));

storiesOf("Button", module)
  .add("with text", () => (
    <Button onClick={action("clicked")}>Hello Button</Button>
  ))
  .add("with some emoji", () => (
    <Button onClick={action("clicked")}>
      <span role="img" aria-label="so cool">
        😀 😎 👍 💯
      </span>
    </Button>
  ));

storiesOf("HelloWorld", module)
  .add("Welcome", () => (
    <HelloWorld />
  ));

storiesOf("Box", module)
  .add("Pruebita de boxes", () => (
    <Box defaultTitleWrapper="Culo" type="blue" />
  ))
  .add("Pruebita de boxes naranja", () => (
    <Box defaultTitleWrapper="Culo" type="orange" />
  ));

  storiesOf("Banner", module)
  .add("Pruebita de banner", () => (
    <Box defaultTitleWrapper="papas con mojo" />
  ));

  storiesOf("Link", module)
  .addDecorator(story => (
    <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
))
  .add("Pruebita de amor", () => (
    <LinkStyle disabled={false} text="esto es un link" to="" />
  ))

  storiesOf("Bar", module)
  .add("Pruebita de bar", () => (
    <Bar label="patatas con queso" />
  ));

  